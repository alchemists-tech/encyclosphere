# Browser Extensions

This extension injects results from the [Encyclosphere](https://encyclosphere.org) into search engine pages. Results are provided by [EncycloSearch](https://encyclosearch.org).

By default, articles are displayed using a built-in, peer-to-peer reader.

## Installation
The Encyclosphere browser extension is currently only available for Chrome. To install it:
1. Go to chrome://extensions (link is not clickable; copy and paste the URL into the address bar).
2. Enable developer mode using the switch in the top right.
3. Reload the page (press Ctrl+R or F5).
4. [Click here](https://gitlab.com/ks_found/browser-extensions/-/raw/main/Chrome/Encyclosphere.zip) to download the extension.
5. Drag and drop the `Encyclosphere.zip` file into the extensions page you opened earlier.
6. If that didn't work, try extracting the zip file. If the extracted files were not put in a folder automatically, create a new folder and put the extracted files in it.
7. On the extensions page, click on "Load unpacked" in the top left, and select the folder containing the extracted files.
8. To open the settings page, click on the puzzle piece icon in the top right. Click on the "Encyclosphere" item.
