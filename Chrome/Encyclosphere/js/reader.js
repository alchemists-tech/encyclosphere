const searchParams = new URLSearchParams(window.location.search);
const client = new WebTorrent();
let publishers = {};
let options = {};

$(document).ready(async function() {
    options = await loadData();

    if (!searchParams.has("url")) {
        alert("Error: No URL given");
        $("#preloader").hide();
        return;
    }
    const url = searchParams.get("url");

    setTimeout(function() {
        if ($("#preloader").length) {
            $("#reloadBtn").click(() => window.location.reload());
            $("#originalSourceBtn").attr("href", url);
            $("#notLoadingMessage").show();
        }
    }, 6000);

    $.get("https://encyclosearch.org/encyclosphere/publishers", function(data) {
        for (publisher of data) {
            publishers[publisher.ID] = {};
            publishers[publisher.ID].name = publisher.Name;
            publishers[publisher.ID].homepage = publisher.Homepage;
        }
    });

    $.get(`https://encyclosearch.org/encyclosphere/exists?url=${url}`, function(exists) {
        if (exists === "true") {
            loadReader(url);
        } else {
            $.get(`https://encyclosearch.org/canGenerate?url=${url}`, function(canGenerate) {
                $("#progressText").text("Generating ZWI file, this might take a while...");
                $("#progressBar").hide();
                $("#progressPercentage").hide();
                $("#spinner").show();
                if (canGenerate === "true") loadReader(url);
                else window.location = url;
            });
        }
    });
});

function loadReader(url) {
    client.on("error", () => window.location = url);
    client.add(`https://encyclosearch.org/encyclosphere/download/torrent?url=${url}${searchParams.get("webseeds") === "false" ? "&webseeds=false" : ""}`, async torrent => {
        $("#progressBar").show();
        $("#progressPercentage").show();
        $("#spinner").hide();
        $("#progressText").text("Loading article...");
        torrent.on("download", () => {
            const width = torrent.progress * 100;
            const progressBar = $(".progress-bar");
            progressBar.attr("aria-valuenow", width);
            progressBar.css("width", `${width}%`);
            $("#progressPercentage").text(`${Math.round(width)}%`);
        });

        const file = torrent.files[0];
        const blob = await getZWIBlob(file, url);
        const zwi = await ZWIFile.fromBlob(blob);
        const html = await zwi.files["article.html"];

        if (html === undefined) {
            window.location = url;
            return;
        }

        const article = $(await html.text());

        article.find("img").each(async function() {
            $(this).attr("src", await blobToBase64(zwi.files[$(this).attr("src")]));
        });

        // Make relative links into absolute links
        article.find("a").each(function() {
            let href = $(this).attr("href");
            if (href !== undefined && href.startsWith("/")) {
                href = `https://${url.trim().replace(/.*:\/\//, "").split("/")[0]}${href}`;
                $(this).attr("href", href);
            }
        });

        // Insert the CSS
        for (element of article) {
            if ($(element).prop("tagName") === "LINK" && $(element).attr("href") !== "data/css/design.css") {
                const css = await zwi.files[$(element).attr("href")];
                if (css !== undefined) $(document.head).append($(`<style>${css.text()}</style>`));
            }
        }

        // Add the article title, a link to the original source, and download, theme, and font buttons
        const publisher = publishers[zwi.metadata.Publisher];
        var articleInfo = $(`
                            <div id="articleInfoBox">
                              <div class="d-flex align-items-center">
                                <h1 style="margin-bottom: -6px" id="articleTitle">${zwi.metadata.Title.replaceAll("_", " ")}</h1>
                                <button id="downloadZWIBtn" class="btn btn-flat" style="margin-left: auto; margin-bottom: -26px;">
                                  <i class="bi bi-download" style="font-size: 24px"></i>
                                </button>
                                <div class="dropdown" style="margin-bottom: -25px;">
                                  <button id="themeBtn" class="btn btn-flat" data-bs-toggle="dropdown">
                                    <i class="bi bi-palette" style="font-size: 24px"></i>
                                  </button>
                                  <ul class="dropdown-menu">
                                    <li><a id="lightThemeBtn" class="dropdown-item" href="#"><i class="bi bi-circle-fill circle-light"></i> Light ${options.theme === "light" ? '<i class="bi bi-check2"></i>' : ""}</a></li>
                                    <li><a id="darkThemeBtn" class="dropdown-item" href="#"><i class="bi bi-circle-fill circle-dark"></i> Dark ${options.theme === "dark" ? '<i class="bi bi-check2"></i>' : ""}</a></li>
                                    <li><a id="sepiaThemeBtn" class="dropdown-item" href="#"><i class="bi bi-circle-fill circle-sepia"></i> Sepia ${options.theme === "sepia" ? '<i class="bi bi-check2"></i>' : ""}</a></li>
                                    <li><a id="systemThemeBtn" class="dropdown-item" href="#"><i class="bi bi-circle-fill circle-system"></i> Default ${options.theme === "system" ? '<i class="bi bi-check2"></i>' : ""}</a></li>
                                  </ul>
                                </div>
                                <div class="dropdown" style="margin-bottom: -25px;">
                                  <button id="fontBtn" class="btn btn-flat" data-bs-toggle="dropdown">
                                    <i class="bi bi-type" style="font-size: 24px"></i>
                                  </button>
                                  <ul class="dropdown-menu">
                                    <li><a id="interFontBtn" class="dropdown-item" href="#" style="font-family: 'Inter', sans-serif;">Inter ${options.font === "inter" ? '<i class="bi bi-check2"></i>' : ""}</a></li>
                                    <li><a id="sourceSansFontBtn" class="dropdown-item" href="#" style="font-family: 'Source Sans Pro', sans-serif;">Source Sans Pro ${options.font === "sourceSans" ? '<i class="bi bi-check2"></i>' : ""}</a></li>
                                    <li><a id="baskervilleFontBtn" class="dropdown-item" href="#" style="font-family: 'Libre Baskerville', serif;">Libre Baskerville ${options.font === "baskerville" ? '<i class="bi bi-check2"></i>' : ""}</a></li>
                                    <li><a id="systemFontBtn" class="dropdown-item" href="#" style="font-family: sans-serif;">System default ${options.font === "system" ? '<i class="bi bi-check2"></i>' : ""}</a></li>
                                  </ul>
                                </div>
                              </div>
                              <hr>
                              <div class="d-flex">
                                <img draggable="false" id="encyclopediaLogo" src="https://encyclosearch.org/img/${zwi.metadata.Publisher}.png" alt="${publisher.name}">
                                <div class="d-flex flex-column" style="display: inline-flex">
                                  <h4 id="articleSubtitle" style="display: inline-block">
                                    From <a href="${publisher.homepage}">${publisher.name}</a>
                                  </h4>
                                  <a id="articleSourceLink" href="${zwi.metadata.SourceURL}">Original source <i class="bi bi-box-arrow-up-right"></i></a>
                                </div>
                              </div>
                            </div>
                            `);
        const container = $("#container");
        container.html(articleInfo);
        container.append(article);

        $("#downloadZWIBtn").click(() => {
            const blobURL = window.URL.createObjectURL(blob);
            const a = document.createElement("a");
            a.href = blobURL;
            a.download = `${zwi.metadata.Title}.zwi`;
            document.body.appendChild(a);
            a.click();
            URL.revokeObjectURL(blobURL);
            a.remove();
        });

        createTooltip("#downloadZWIBtn", "Download ZWI file");
        createTooltip("#themeBtn", "Theme");
        createTooltip("#fontBtn", "Font");

        $("#lightThemeBtn").click(() => setTheme("light"));
        $("#darkThemeBtn").click(() => setTheme("dark"));
        $("#sepiaThemeBtn").click(() => setTheme("sepia"));
        $("#systemThemeBtn").click(() => setTheme("system"));

        $("#interFontBtn").click(() => setFont("inter"));
        $("#sourceSansFontBtn").click(() => setFont("sourceSans"));
        $("#baskervilleFontBtn").click(() => setFont("baskerville"));
        $("#systemFontBtn").click(() => setFont("system"));

        $(".container-fluid").on("click", "a", function(event) {
            const elem = $(this);
            const href = $(this).attr("href");
            if (!href.startsWith("#") && !href.includes("File:") && !($(this).attr("id") === "articleSourceLink")) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                $.get(`https://encyclosearch.org/encyclosphere/exists?url=${href}`, function(exists) {
                    if (exists === "true") {
                        window.location = `/reader.html?url=${href}`;
                        return;
                    }
                    $.get(`https://encyclosearch.org/canGenerate?url=${href}`, function(canGenerate) {
                        if (canGenerate === "true") {
                            const tooltip = new bootstrap.Tooltip(elem, { html: true, trigger: "manual" });
                            tooltip.setContent({ ".tooltip-inner": `
                                                                   <div class="spinner-border spinner-border-sm" role="status"></div>
                                                                   <span style="margin-left: 8px">Generating ZWI file (this might take a while)...</span>
                                                                   ` });
                            tooltip.show();
                            $.get(`https://encyclosearch.org/reader?url=${href}&ignoreView=true`, function() {
                                tooltip.dispose();
                                window.location = `/reader.html?url=${href}`;
                            }).fail(function() {
                                tooltip.dispose();
                                window.location = href;
                            });
                        }
                        else window.location = href;
                    });
                });
            }
        });

        $(".mwe-math-fallback-image-inline").remove();
        $(".mwe-math-mathml-inline").show();

        MathJax._.mathjax.mathjax.handleRetriesFor(() => {
            MathJax.typeset();
        });

        if (window.location.hash.length) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(document.getElementById(window.location.hash.substring(1))).offset().top - 15
            });
        }

        /*
        <i class="bi bi-check2" style="font-size: 24px; color: green"></i>
                                new bootstrap.Tooltip($('#articleSubtitle svg'), { placement: 'right' });*/
    });
}

function createTooltip(selector, tooltipText) {
    new bootstrap.Tooltip($(selector), { title: tooltipText });
}

function setTheme(theme) {
    options.theme = theme;
    chrome.storage.sync.set({options});
    window.location.reload();
}

function setFont(font) {
    options.font = font;
    chrome.storage.sync.set({options});
    window.location.reload();
}

// https://github.com/feross/stream-to-blob
async function getZWIBlob(file, url) {
  return new Promise(async (resolve, reject) => {
      const timeoutID = setTimeout(async () => {
          console.log("Loading from EncycloSearch directly");
          fetch(`https://encyclosearch.org/downloadArticle?url=${url}`)
              .then(async response => {
                  if (!response.ok) window.location = url; // If the download failed, go to the original source
                  const blob = await response.blob();
                  resolve(blob);
              });
      }, 5000);
      const stream = await file.createReadStream();
      const chunks = [];
      stream
          .on("data", chunk => chunks.push(chunk))
          .once("end", () => {
              clearTimeout(timeoutID);
              resolve(new Blob(chunks));
          })
          .once("error", reject);
  });
}

// https://stackoverflow.com/a/18650249/5905216
function blobToBase64(blob) {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.readAsDataURL(blob);
  });
}
