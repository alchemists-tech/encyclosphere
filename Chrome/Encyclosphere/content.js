let url;
let duckDuckGo, google, googleAlt;
let options = {};
let client;
let resultsInjected = false;

const query = new URLSearchParams(window.location.search).get("q");

load();

async function load() {
    options = await loadData(true);

    url = decodeURIComponent(location.href);
    if ((isDuckDuckGo(url) && options.engines.duckDuckGo) || (isGoogle(url) && options.engines.google)) {
        duckDuckGo = isDuckDuckGo(url);
        google = isGoogle(url);

        let watchingFor;
        if (duckDuckGo) watchingFor = "article";
        else if (google) watchingFor = "#rso > div";

        await waitForElm(watchingFor);

        let id = setInterval(async function() {
            if (document.querySelectorAll(watchingFor).length) {
                clearInterval(id);

                if (options.wikipediaToReader) repointWikipedia();

                if (document.querySelector("#rcnt > .XqFnDf > div > .kp-wholepage-osrp") != null) {
                    google = false;
                    googleAlt = true;
                }

                if (options.resultGetButton || shouldInjectResults()) prepareContent();

                if (shouldInjectResults()) injectResults();
                else {
                    if (options.resultGetButton) setToggleText("Get Encyclosphere results");
                    if (options.removeWikipedia !== "none") removeWikipedia(() => true);
                }
            }
        }, 500);
    }
}

function shouldInjectResults() {
    if ((options.showResults === "ifWikipedia" || options.showResults === "ifWikipediaTop3") && hasFeaturedWikipediaResult()) return true;

    let selector;
    if (duckDuckGo) selector = '.react-results--main > li[data-layout="organic"] > article .ikg2IXiCD14iVX7AdZo1 a';
    else if (google || googleAlt) selector = ".yuRUbf a:not(.Wt5Tfe a)";

    switch (options.showResults) {
        case "always":
            return true;
        case "ifWikipedia":
            const resultLinks = document.querySelectorAll(selector);
            for (const link of resultLinks) {
                if (isWikipedia(link.getAttribute("href"))) return true;
            }
            return false;
        case "ifWikipediaTop3":
            const resultLinks2 = document.querySelectorAll(selector);
            for (let i = 0; i < resultLinks2.length; i++) {
                const link = resultLinks2[i];
                if (i < 3 && isWikipedia(link.getAttribute("href"))) return true;
            }
            return false;
        case "never":
            return false;
    }
}

function removeWikipedia(resultsDoNotContain) {
    if (hasFeaturedWikipediaResult()) {
        let remove = true;
        if (options.removeWikipedia === "exactMatches") {
            const featuredText = document.querySelector("#Odp5De .MBeuO").textContent.split(" - ")[0];
            if (resultsDoNotContain(featuredText)) remove = false;
        }
        const featuredResult = document.querySelector("#Odp5De");
        if (remove) featuredResult.parentNode.removeChild(featuredResult);
    }

    let resultSelector;

    if (duckDuckGo) resultSelector = ".react-results--main li";
    else if (google) resultSelector = "#rso .MjjYud";
    else if (googleAlt) resultSelector = "#rso .sATSHe > div:first-child";

    const results = document.querySelectorAll(resultSelector);
    for (const result of results) {
        let aSelector;

        if (duckDuckGo) aSelector = ".ikg2IXiCD14iVX7AdZo1 a";
        else if (google || googleAlt) aSelector = ".yuRUbf a";

        const a = result.querySelector(aSelector);
        if (a === null) continue;
        const url = a.getAttribute("href");
        if (isWikipedia(url)) {
            if (options.removeWikipedia === "exactMatches") {
                let titleSelector;
                if (duckDuckGo) titleSelector = ".EKtkFWMYpwzMKOYr0GYm";
                else if (google || googleAlt) titleSelector = ".MBeuO";

                const title = result.querySelector(titleSelector).textContent.split(" - ")[0];
                if (resultsDoNotContain(title)) continue;
            }
            result.parentNode.removeChild(result);
            //if (duckDuckGo) result.parentNode.removeChild(result);
            //else if (google || googleAlt) result.parentNode.removeChild(result);
        }
    }
}

function repointWikipedia() {
    const links = document.querySelectorAll("a");
    for (const link of links) {
        const href = link.getAttribute("href");
        if (!isWikipedia(href)) continue;

        switch (options.resultLinks) {
            case "reader":
                url = `${chrome.runtime.getURL("reader.html")}?url=${href}`;
                break;
            case "encycloSearch":
                url = `https://encyclosearch.org/reader?url=${href}`;
                break;
            case "encycloReader":
                url = `https://encycloreader.org/r/wikipedia.php?q=${href.split("/wiki/")[1]}`
                break;
        }
        link.setAttribute("href", url);
    }
}

function hasFeaturedWikipediaResult() {
    return google && document.querySelector("#Odp5De") != null && isWikipedia(document.querySelector("#Odp5De .yuRUbf a").getAttribute("href"));
}

var contentContainer, resultContainer, resultToggle;

function prepareContent() {
    contentContainer = document.createElement("div");
    contentContainer.id = "encyclosphereContentContainer";

    const style = document.createElement("link");
    style.rel = "stylesheet";
    style.href = chrome.runtime.getURL(`css/search/${google || googleAlt ? "google" : "duckduckgo"}.css`);
    contentContainer.append(style);

    resultToggle = document.createElement("a");
    resultToggle.id = "encyclosphereResultToggle";
    resultToggle.href = "#";
    resultToggle.onclick = function(e) {
        e.preventDefault();
        if (resultContainer.hidden) {
            injectResults();
            showResults();
        } else {
            hideResults();
        }
    };
    contentContainer.append(resultToggle);

    resultContainer = document.createElement("div");
    resultContainer.id = "encyclosphereResultContainer";
    resultContainer.innerHTML = '<p id="encyclosphereLoader">Loading...</p>';
    if (!shouldInjectResults()) hideResults();
    else showResults();
    contentContainer.append(resultContainer);

    let resultParent;
    if (duckDuckGo) resultParent = ".react-results--main";
    else if (google || googleAlt) resultParent = "#rso";
    document.querySelector(resultParent).prepend(contentContainer);
}

function showResults() {
    resultContainer.hidden = false;
    setToggleText(`Encyclosphere results <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16" style="position: relative; bottom: -2px; margin-left: 2px">
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
  <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
</svg>`);
    resultToggle.classList.add("enc-toggle-visible");
}

function hideResults() {
    resultContainer.hidden = true;
    setToggleText("Show Encyclosphere results");
    resultToggle.classList.remove("enc-toggle-visible");
}

function setToggleText(toggleText) {
    resultToggle.innerHTML = `<img src="${chrome.runtime.getURL("logos/logo-48px-white.png")}" alt="Encyclosphere logo" width="20" height="20" loading="lazy" /> ${toggleText}`;
}

async function injectResults() {
    if (resultsInjected) return;
    resultsInjected = true;
    const response = await fetch(`https://encyclosearch.org/encyclosphere/search?q=${query}&page=1&resultsPerPage=${options.resultsCount}&excludedPublishers=wikipedia&includePublisherDetails=true`);
    const results = await response.json();
    if (options.removeWikipedia !== "none" && (!options.noResultsNoRemoveWikipedia || results.Results.length > 0)) {
        removeWikipedia(text => {
            for (result of results.Results) {
                if (result.Title.trim().toLowerCase() === text.trim().toLowerCase()) return false;
            }
            return true;
        });
    }

    resultContainer.innerHTML = "";

    if (results.Results.length === 0) {
        const noResultsText = document.createElement("a");
        noResultsText.id = "encyclosphereNoResultsText";
        noResultsText.innerHTML = `<img src="${chrome.runtime.getURL("logos/logo-48px.png")}" alt="No results" width="20" height="20" loading="lazy" /> No results`;
        resultContainer.remove();
        resultToggle.remove();
        contentContainer.append(noResultsText);
        return;
    }

    for (result of results.Results) {
        const imageResponse = await fetch(`https://encyclosearch.org/img/${result.Publisher}.png`);
        const blob = await imageResponse.blob();
        const image = await blobToData(blob);

        let newURL;
        if (!result.ReaderSupported) {
            newURL = result.SourceURL;
        } else {
            switch (options.resultLinks) {
                case "originalSource":
                    newURL = result.SourceURL;
                    break;
                case "reader":
                    newURL = `${chrome.runtime.getURL("reader.html")}?url=${result.SourceURL}`;
                    break;
                case "encycloSearch":
                    newURL = `https://encyclosearch.org/reader?url=${result.SourceURL}`;
                    break;
                case "encycloReader":
                    newURL = `https://encycloreader.org/r/${result.Publisher}.php?q=${result.Title.replaceAll(" ", "_")}`
                    break;
            }
        }

        const newResult = document.createElement("div");

        if (duckDuckGo) {
            newResult.innerHTML = `
                                  <article data-handled-by-react="true"
                                                data-testid="result"
                                                data-nrn="result"
                                                class="yQDlj3B5DI5YO8c8Ulio CpkrTDP54mqzpuCSn1Fa SKlplDuh9FjtDprgoMxk">
                                    <div class="OQ_6vPwNhCeusNiEDcGp">
                                      <div class="mwuQiMOjmFJ5vmN6Vcqw NvMwcsUp56q4W2Z_b8E7 hAeZQDlu0XXeGwL7U722 SgSTKoqQXa0tEszD2zWF LQVY1Jpkk8nyJ6HBWKAk">
                                        <span class="DpVR46dTZaePK29PDkz8" style="margin-right: 0.25em">
                                          <img src="${image}" width="16" height="16" loading="lazy" style="background-color: lightgray" />
                                        </span>
                                        <a href="${newURL}"
                                           rel="noopener"
                                           target="_self"
                                           data-testid="result-extras-url-link"
                                           class="Rn_JXVtoPVAFyGkcaXyK"
                                           data-handled-by-react="true">${result.PublisherName} - From the Encyclosphere <img src="${chrome.runtime.getURL("logos/logo-48px.png")}" width="20" height="20" loading="lazy" style="margin-bottom: -6px" /></a>
                                      </div>
                                    </div>
                                    <div class="ikg2IXiCD14iVX7AdZo1">
                                      <h2 class="LnpumSThxEWMIsDdAT17 CXMyPcQ6nDv47DKFeywM">
                                        <a href="${newURL}"
                                           rel="noopener"
                                           target="_self"
                                           class="eVNpHGjtxRBq_gLOfGDr LQNqh2U1kzYxREs65IJu"
                                           data-testid="result-title-a"
                                           data-handled-by-react="true">
                                          <span class="EKtkFWMYpwzMKOYr0GYm LQVY1Jpkk8nyJ6HBWKAk">${result.Title}</span>
                                        </a>
                                      </h2>
                                    </div>
                                    <div class="E2eLOJr8HctVnDOTM8fs">
                                      <div class="OgdwYG6KE2qthn9XQWFC">
                                        <span>${result.Description.length > 350 ? result.Description.slice(0, 350) + "..." : result.Description}</span>
                                      </div>
                                    </div>
                                  </article>
                                  `;
        } else if (google || googleAlt) {
            newResult.className = "MjjYud";
            newResult.innerHTML = `
                                  <div class="g Ww4FFb vt6azd tF2Cxc" lang="en" style="width:600px">
                                    <div class="kvH3mc BToiNc UK95Uc">
                                      <div class="Z26q7c UK95Uc jGGQ5e" data-header-feature="0">
                                        <div class="yuRUbf">
                                          <a href="${newURL}">
                                            <br>
                                            <h3 class="LC20lb MBeuO DKV0Md" style="margin-top: 0">${result.Title}</h3>
                                            <div class="TbwUpd NJjxre">
                                              <cite class="iUh30 tjvcx" style="font-size: 14px" role="text">
                                                <img src="${image}" width="16" height="16" loading="lazy" style="background-color: lightgray; margin-bottom: -3px; margin-right: 3px" />
                                                ${result.PublisherName} - From the Encyclosphere 
                                                <img src="${chrome.runtime.getURL("logos/logo-48px.png")}" width="20" height="20" loading="lazy" style="margin-bottom: -6px" />
                                              </cite>
                                            </div>
                                          </a>
                                        </div>
                                      </div>
                                      <div class="Z26q7c UK95Uc" data-content-feature="1">
                                        <div class="VwiC3b yXK7lf MUxGbd yDYNvb lyLwlc lEBKkf" style="-webkit-line-clamp:2">
                                          <span>${result.Description.length > 350 ? result.Description.slice(0, 350) + "..." : result.Description}</span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  `;
        }
        resultContainer.append(newResult);
    }

    const moreResultsBtn = document.createElement("a");
    moreResultsBtn.innerText = "More results";
    moreResultsBtn.id = "encyclosphereMoreResultsBtn";
    moreResultsBtn.href = `https://encyclosearch.org/?q=${encodeURIComponent(query)}`;
    resultContainer.append(moreResultsBtn);
}

function isWikipedia(url) {
    return /(https:\/\/)?.*\.wikipedia.org\/.*/.test(url);
}

function isDuckDuckGo(url) {
    return url.startsWith("https://duckduckgo.com");
}

function isGoogle(url) {
    return url.startsWith("https://www.google.com/search");
}

// https://stackoverflow.com/a/61511955/5905216
function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve();
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve();
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}

// https://github.com/feross/stream-to-blob
async function streamToBlob(stream, mimeType) {
  if (mimeType != null && typeof mimeType !== "string") {
    throw new Error("Invalid mimetype, expected string.");
  }
  return new Promise((resolve, reject) => {
    const chunks = [];
    stream
      .on("data", chunk => chunks.push(chunk))
      .once("end", () => {
        const blob = mimeType != null
          ? new Blob(chunks, { type: mimeType })
          : new Blob(chunks);
        resolve(blob);
      })
      .once("error", reject);
  })
}

// https://stackoverflow.com/a/63372663/5905216
function blobToData(blob) {
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result)
    reader.readAsDataURL(blob)
  });
}
